package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.system.domain.Visitor;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.VisitorVo;
import com.ruoyi.system.domain.bo.VisitorBo;
import com.ruoyi.system.service.IVisitorService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【请填写功能名称】
 *
 * @author ruoyi
 * @date 2023-12-05
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/visitor")
public class VisitorController extends BaseController {

    private final IVisitorService iVisitorService;

    /**
     * 查询【请填写功能名称】列表
     */
//    @SaCheckPermission("system:visitor:list")
    @GetMapping("/list")
    public TableDataInfo<Visitor> list(VisitorBo bo, PageQuery pageQuery) {
        return iVisitorService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @SaCheckPermission("system:visitor:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(VisitorBo bo, HttpServletResponse response) {
        List<VisitorVo> list = iVisitorService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", VisitorVo.class, response);
    }

    /**
     * 获取【请填写功能名称】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:visitor:query")
    @GetMapping("/{id}")
    public R<VisitorVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iVisitorService.queryById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @SaCheckPermission("system:visitor:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody Visitor visitor) {
        return toAjax(iVisitorService.insertByVisitor(visitor));
    }

    /**
     * 修改【请填写功能名称】
     */
    @SaCheckPermission("system:visitor:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody VisitorBo bo) {
        return toAjax(iVisitorService.updateByBo(bo));
    }

    /**
     * 删除【请填写功能名称】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:visitor:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iVisitorService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
