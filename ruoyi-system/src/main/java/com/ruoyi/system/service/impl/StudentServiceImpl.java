package com.ruoyi.system.service.impl;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.annotation.Translation;
import com.ruoyi.common.constant.ApartmentConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StreamUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.ApartmentMapper;
import com.ruoyi.system.mapper.SysUserRoleMapper;
import com.ruoyi.system.service.ISysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.StudentBo;
import com.ruoyi.system.domain.vo.StudentVo;
import com.ruoyi.system.domain.Student;
import com.ruoyi.system.mapper.StudentMapper;
import com.ruoyi.system.service.IStudentService;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-11-28
 */
@RequiredArgsConstructor
@Service
public class StudentServiceImpl implements IStudentService {

    private final StudentMapper baseMapper;

    private final StudentMapper studentMapper;

    private final SysUserRoleMapper userRoleMapper;

    private final ApartmentMapper apartmentMapper;

    @Autowired
    private ISysUserService iSysUserService;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public StudentVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<StudentVo> queryPageList(StudentBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Student> lqw = buildQueryWrapper(bo);
        Page<StudentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<StudentVo> queryList(StudentBo bo) {
        LambdaQueryWrapper<Student> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Student> buildQueryWrapper(StudentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Student> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getStudentId() != null, Student::getStudentId, bo.getStudentId());
        lqw.like(StringUtils.isNotBlank(bo.getName()), Student::getName, bo.getName());
        lqw.like(StringUtils.isNotBlank(bo.getStatus()), Student::getStatus,bo.getStatus());
        lqw.eq(bo.getAge() != null, Student::getAge, bo.getAge());
        lqw.eq(StringUtils.isNotBlank(bo.getSex()), Student::getSex, bo.getSex());
        lqw.eq(StringUtils.isNotBlank(bo.getDormitory()), Student::getDormitory, bo.getDormitory());
        lqw.eq(bo.getDormitoryNumber() != null, Student::getDormitoryNumber, bo.getDormitoryNumber());
        lqw.eq(StringUtils.isNotBlank(bo.getPhone()), Student::getPhone, bo.getPhone());
        lqw.eq(StringUtils.isNotBlank(bo.getDepartment()), Student::getDepartment, bo.getDepartment());
        lqw.eq(StringUtils.isNotBlank(bo.getSpeciality()), Student::getSpeciality, bo.getSpeciality());
        lqw.eq(StringUtils.isNotBlank(bo.getClassStudent()), Student::getClassStudent, bo.getClassStudent());
        return lqw;
    }

    /**
     * 新增【】
     */
    @Override
    @Transactional
    public Boolean insertByBo(Student student) {
//        long variable = 123;
//        student.setId(variable);
        Student add = BeanUtil.toBean(student, Student.class);
        validEntityBeforeSave(add);
        boolean flag = studentMapper.insertStudent(add);
        Map<String, Object> param = new HashMap<>();
        param.put("dormitory",student.getDormitory());
        param.put("floor",student.getFloor());
        param.put("roomNuber",student.getDormitoryNumber());
        // 根据宿舍楼、楼层、宿舍编号查询该宿舍的剩余床位
        Apartment apartment = apartmentMapper.selectApartByFloor(param);
        Integer sy = apartment.getSycw();
//        int i = Integer.parseInt(sy);
        Integer sycw = sy - 1;
        // 修改剩余床位
        boolean i1 = apartmentMapper.updateSycwById(apartment.getId(),sycw)>0;

//        boolean flag = baseMapper.insert(add) > 0;
        if (i1) {
            student.setId(add.getId());
            // sys_user表同步数据
            SysUser sysUser = new SysUser();
            sysUser.setUserName(student.getStudentId().toString()); // 账号
            sysUser.setPassword(BCrypt.hashpw("admin123"));
            sysUser.setNickName(student.getName());
            sysUser.setPhonenumber(student.getPhone()); // 电话
            sysUser.setEmail(student.getEmail()); // 邮箱
            Long [] roleIds = new Long[1];
            roleIds[0] = 3L;
            sysUser.setRoleIds(roleIds);
            iSysUserService.insertUser(sysUser);
//            int rows = baseMapper.insert(sysUser);
//            // 新增用户与角色管理
//            insertUserRole(sysUser);

        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(Student student) {
        Student update = BeanUtil.toBean(student, Student.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Student entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user) {
        this.insertUserRole(user.getUserId(), user.getRoleIds());
    }

    /**
     * 新增用户角色信息
     *
     * @param userId  用户ID
     * @param roleIds 角色组
     */
    public void insertUserRole(Long userId, Long[] roleIds) {
        if (ArrayUtil.isNotEmpty(roleIds)) {
            // 新增用户与角色管理
            List<SysUserRole> list = StreamUtils.toList(Arrays.asList(roleIds), roleId -> {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(userId);
                ur.setRoleId(roleId);
                return ur;
            });
            userRoleMapper.insertBatch(list);
        }
    }

    @Override
    public boolean checkstudentIdUnique(Student student) {
        Long id = ObjectUtil.isNull(student.getId()) ? -1L : student.getId();
        Student st = studentMapper.checkstudentIdUnique(student.getStudentId());
        if (ObjectUtil.isNotNull(st) && st.getId().longValue() != id.longValue())
        {
            return ApartmentConstants.NOT_UNIQUE;
        }
        return ApartmentConstants.UNIQUE;

//        return false;
    }

    @Override
    public boolean checkPhoneUnique(Student student) {
        Long id = ObjectUtil.isNull(student.getId()) ? -1L : student.getId();
        Student st = studentMapper.checkPhoneUnique(student.getPhone());
        if (ObjectUtil.isNotNull(st) && st.getId().longValue() != id.longValue())
        {
            return ApartmentConstants.NOT_UNIQUE;
        }
        return ApartmentConstants.UNIQUE;
    }

    @Override
    public boolean checkEmailUnique(Student student) {
        Long id = ObjectUtil.isNull(student.getId()) ? -1L : student.getId();
        Student st = studentMapper.checkEmailUnique(student.getEmail());
        if (ObjectUtil.isNotNull(st) && st.getId().longValue() != id.longValue())
        {
            return ApartmentConstants.NOT_UNIQUE;
        }
        return ApartmentConstants.UNIQUE;
    }

    @Override
    public Boolean updateStatusById(Integer id) {
//        try {
//            int ids = Math.toIntExact(id);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        Boolean flag = studentMapper.updateStatusById(id,null);
        return flag;
    }

    @Override
    public Boolean updateStatusById1(Long studentId) {
        Boolean flag = studentMapper.updateStatusById1(studentId);
        return flag;
    }

    @Override
    public Boolean updateStatusById2(Long id, String status) {
        Boolean flag = studentMapper.updateStatusById2(id,status);
        return flag;
    }
}
