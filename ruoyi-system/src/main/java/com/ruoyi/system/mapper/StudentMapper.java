package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.Student;
import com.ruoyi.system.domain.vo.StudentVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-11-28
 */
public interface StudentMapper extends BaseMapperPlus<StudentMapper, Student, StudentVo> {


    /**
     * 新增学生信息
     *
     * @param student 学生信息
     * @return 结果
     */
    public boolean insertStudent(Student student);


    /**
     * 校验学生学号是否唯一
     *
     * @param studentId 学号
     * @return 结果
     */
    public Student checkstudentIdUnique(@Param("studentId")Long studentId);

    /**
     * 校验手机号是否唯一
     *
     * @param phone 手机号
     * @return 结果
     */
    public Student checkPhoneUnique(@Param("phone")String phone);

    /**
     * 校验邮箱是否唯一
     *
     * @param email 邮箱
     * @return 结果
     */
    public Student checkEmailUnique(@Param("email")String email);

    public Boolean updateStatusById(@Param("id")Integer id,@Param("status")String status);

    public Boolean updateStatusById1(@Param("studentId")Long studentId);

    public Student selectStudentList(@Param("studentId")Long studentId);

    public Boolean updateStatusById2(@Param("studentId")Long studentId,@Param("status")String status);


}
