package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 【请填写功能名称】视图对象 visitor
 *
 * @author ruoyi
 * @date 2023-12-05
 */
@Data
@ExcelIgnoreUnannotated
public class VisitorVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Integer id;

    /**
     * 访客姓名
     */
    @ExcelProperty(value = "访客姓名")
    private String fkName;

    /**
     * 访客联系电话
     */
    @ExcelProperty(value = "访客联系电话")
    private String fkPhone;

    /**
     * 寻访人
     */
    @ExcelProperty(value = "寻访人")
    private String xfName;

    /**
     * 寻访时间
     */
    @ExcelProperty(value = "寻访时间")

    private String createTime;





}
