package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.vo.ApartmentVo;
import com.ruoyi.system.domain.bo.ApartmentBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-12-01
 */
public interface IApartmentService {

    /**
     * 查询【请填写功能名称】
     */
    ApartmentVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<ApartmentVo> queryPageList(ApartmentBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<ApartmentVo> queryList(ApartmentBo bo);

    /**
     * 查询字典列表
     */
    List<ApartmentVo> queryList1(Apartment apartment);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(Apartment apartment);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(Apartment apartment);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 校验宿舍楼下楼层是否唯一
     *
     * @param apartment 公寓信息
     * @return 结果
     */
    public boolean checkFloorUnique(Apartment apartment);

    /**
     * 校验宿舍楼下楼层下的宿舍编号是否唯一
     *
     * @param apartment 公寓信息
     * @return 结果
     */
    public boolean checkRoomNuberUnique(Apartment apartment);


    /*
        * 查询字典列表
     */
    List<Apartment> queryList2(String dormitory,String floor);

    public Boolean updateStatusById1(Long studentId);

    public Boolean updateStatusById2(Integer id);
}
