package com.ruoyi.common.utils.face.dto;

import com.ruoyi.common.utils.face.constant.FaceConstant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Descripttion
 * @Author cuihongmin
 * @Date 2023/12/7 10:27
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FaceUserDTO<T> implements Serializable {
    private String userId;


    private String groupId = FaceConstant.DEFAULT_GROUP_ID;

//    private String userId = FaceConstant.USER_ID_LIST;

    private String faceToken;

    private T user;
}
