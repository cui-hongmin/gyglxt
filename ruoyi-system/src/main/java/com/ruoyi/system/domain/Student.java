package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.ruoyi.common.core.domain.entity.SysRole;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 student
 *
 * @author ruoyi
 * @date 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("student2")
public class Student extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
//    @TableId(value = "id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 学号
     */
    private Long studentId;
    /**
     * 姓名
     */
    private String name;

    /**
     * 状态
     */
    private String status;
    /**
     * 年龄
     */
    private Long age;
    /**
     * 性别
     */
    private String sex;
    /**
     * 宿舍楼
     */
    private String dormitory;
    /**
     * 楼层
     */
    private String floor;
    /**
     * 寝室编号
     */
    private Long dormitoryNumber;
    /**
     * 电话
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;
    /**
     * 院系
     */
    private String department;
    /**
     * 专业
     */
    private String speciality;
    /**
     * 班级
     */
    private String classStudent;

    /**
     * 角色对象
     */
    @TableField(exist = false)
    private List<SysRole> roles;

    /**
     * 角色组
     */
    @TableField(exist = false)
    private Long[] roleIds;

    /**
     * 数据权限 当前角色ID
     */
    @TableField(exist = false)
    private Long roleId;

}
