package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.Student;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.StudentVo;
import com.ruoyi.system.domain.bo.StudentBo;
import com.ruoyi.system.service.IStudentService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【学生信息控制层】
 *
 * @author ruoyi
 * @date 2023-11-28
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/student")
public class StudentController extends BaseController {

    private final IStudentService iStudentService;

    /**
     * 查询【学生信息】列表
     */
//    @SaCheckPermission("system:student:list")
    @GetMapping("/list")
    public TableDataInfo<StudentVo> list(StudentBo bo, PageQuery pageQuery) {
        return iStudentService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【学生信息】列表
     */
    @SaCheckPermission("system:student:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(StudentBo bo, HttpServletResponse response) {
        List<StudentVo> list = iStudentService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", StudentVo.class, response);
    }

    /**
     * 获取【学生信】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:student:query")
    @GetMapping("/{id}")
    public R<StudentVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iStudentService.queryById(id));
    }

    /**
     * 新增
     */
    @SaCheckPermission("system:student:add")
    @Log(title = "新增", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody Student student) {
        if (!iStudentService.checkstudentIdUnique(student)) {
            return R.fail("新增学生信息'" + student.getName() + "'失败，学号已存在");
        }
        else if (StringUtils.isNotEmpty(student.getPhone()) && !iStudentService.checkPhoneUnique(student))
        {
            return R.fail("新增学生信息'" + student.getName() + "'失败，电话已存在");
        }
        else if (StringUtils.isNotEmpty(student.getEmail()) && !iStudentService.checkEmailUnique(student)) {
            return R.fail("新增用户'" + student.getName() + "'失败，邮箱账号已存在");
        }
        return toAjax(iStudentService.insertByBo(student));
    }

    /**
     * 修改【学生信息】
     */
    @SaCheckPermission("system:student:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody Student student) {
        if (!iStudentService.checkstudentIdUnique(student)) {
            return R.fail("修改学生信息'" + student.getName() + "'失败，该学号已存在");
        }
        else if (StringUtils.isNotEmpty(student.getPhone()) && !iStudentService.checkPhoneUnique(student))
        {
            return R.fail("修改学生信息'" + student.getName() + "'失败，该电话已存在");
        }
        else if (StringUtils.isNotEmpty(student.getEmail()) && !iStudentService.checkEmailUnique(student)) {
            return R.fail("修改用户'" + student.getName() + "'失败，该邮箱账号已存在");
        }
        return toAjax(iStudentService.updateByBo(student));
    }

    /**
     * 删除【学生信息】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:student:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iStudentService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 销假
     */
    @GetMapping("/xj/{id}")
    public R<Void> edit(@NotNull(message = "id不能为空") @PathVariable Integer id) {
        if (id != null) {
            return toAjax(iStudentService.updateStatusById(id));
        }
        return R.fail(501,"学生id不能为空");

    }

    /**
     * 修改学生状态为请假状态
     */
    @GetMapping("/updateStstus/{studentId}")
    public R<Void> edit1(@NotNull(message = "学号不能为空") @PathVariable Long studentId) {
        if (studentId != null) {
            return toAjax(iStudentService.updateStatusById1(studentId));
        }
        return R.fail(501,"学生id不能为空");

    }



}
