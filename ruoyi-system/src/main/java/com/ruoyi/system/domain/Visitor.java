package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 visitor
 *
 * @author ruoyi
 * @date 2023-12-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("visitor")
public class Visitor extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 访客姓名
     */
    private String fkName;
    /**
     * 访客联系电话
     */
    private String fkPhone;
    /**
     * 寻访人
     */
    private String xfName;

    private Date createTime;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
