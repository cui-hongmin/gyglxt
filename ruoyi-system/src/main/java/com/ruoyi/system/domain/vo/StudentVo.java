package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 【请填写功能名称】视图对象 student
 *
 * @author ruoyi
 * @date 2023-11-28
 */
@Data
@ExcelIgnoreUnannotated
public class StudentVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Integer id;

    /**
     * 学号
     */
    @ExcelProperty(value = "学号")
    private Long studentId;

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名")
    private String name;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态")
    private String status;

    /**
     * 年龄
     */
    @ExcelProperty(value = "年龄")
    private Long age;

    /**
     * 性别
     */
    @ExcelProperty(value = "性别")
    private String sex;

    /**
     * 宿舍楼
     */
    @ExcelProperty(value = "宿舍楼")
    private String dormitory;

    /**
     * 楼层
     */
    @ExcelProperty(value = "楼层")
    private String floor;

    /**
     * 寝室编号
     */
    @ExcelProperty(value = "寝室编号")
    private Long dormitoryNumber;

    /**
     * 电话
     */
    @ExcelProperty(value = "电话")
    private String phone;

    /**
     * 邮箱
     */
    @ExcelProperty(value = "邮箱")
    private String email;

    /**
     * 院系
     */
    @ExcelProperty(value = "院系")
    private String department;

    /**
     * 专业
     */
    @ExcelProperty(value = "专业")
    private String speciality;

    /**
     * 班级
     */
    @ExcelProperty(value = "班级")
    private String classStudent;


}
