package com.ruoyi.system.service;

import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.Student;
import com.ruoyi.system.domain.vo.StudentVo;
import com.ruoyi.system.domain.bo.StudentBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-11-28
 */
public interface IStudentService {

    /**
     * 查询【请填写功能名称】
     */
    StudentVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<StudentVo> queryPageList(StudentBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<StudentVo> queryList(StudentBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByBo(Student student);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(Student student);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 校验学生信息学号是否唯一
     *
     * @param student 学生信息
     * @return 结果
     */
    public boolean checkstudentIdUnique(Student student);

    /**
     * 校验学生信息电话是否唯一
     *
     * @param student 学生信息
     * @return 结果
     */
    public boolean checkPhoneUnique(Student student);

    /**
     * 校验学生信息邮箱是否唯一
     *
     * @param student 学生信息
     * @return 结果
     */
    public boolean checkEmailUnique(Student student);

    public Boolean updateStatusById(Integer id);

    public Boolean updateStatusById1(Long studentId);

    // 修改学生状态为在宿
    public Boolean updateStatusById2(Long id,String status);
}
