package com.ruoyi.web.controller.system;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.Apartment;
import com.ruoyi.workflow.domain.vo.WfCategoryVo;
import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.PageQuery;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import com.ruoyi.common.core.validate.QueryGroup;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.vo.ApartmentVo;
import com.ruoyi.system.domain.bo.ApartmentBo;
import com.ruoyi.system.service.IApartmentService;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 【公寓房间管理控制层】
 *
 * @author cuihongmin
 * @date 2023-12-01
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/system/apartment")
public class ApartmentController extends BaseController {

    private final IApartmentService iApartmentService;

    /**
     * 查询【公寓房间】列表
     */
    @SaCheckPermission("system:apartment:list")
    @GetMapping("/list")
    public TableDataInfo<ApartmentVo> list(ApartmentBo bo, PageQuery pageQuery) {
        return iApartmentService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出【公寓房间信息】列表
     */
    @SaCheckPermission("system:apartment:export")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ApartmentBo bo, HttpServletResponse response) {
        List<ApartmentVo> list = iApartmentService.queryList(bo);
        ExcelUtil.exportExcel(list, "【请填写功能名称】", ApartmentVo.class, response);
    }

    /**
     * 获取【公寓房间】详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("system:apartment:query")
    @GetMapping("/{id}")
    public R<ApartmentVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iApartmentService.queryById(id));
    }

    /**
     * 新增【公寓房间信息】
     */
    @SaCheckPermission("system:apartment:add")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody Apartment apartment) {
        if (StringUtils.isNotEmpty(apartment.getRoomNuber()) && !iApartmentService.checkRoomNuberUnique(apartment)) {
            return R.fail("新增宿舍'" + apartment.getDormitory() + "'失败，'" +apartment.getFloor() + "'楼层'" +apartment.getRoomNuber() +"'门牌号已存在");
        }
//        else if (!iApartmentService.checkFloorUnique(apartment))
//        {
//            return R.fail("新增宿舍'" + apartment.getDormitory() + "'失败，'" +apartment.getFloor() + "'楼层已存在");
//        }
        return toAjax(iApartmentService.insertByBo(apartment));
    }

    /**
     * 修改【公寓房间信息】
     */
    @SaCheckPermission("system:apartment:edit")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody Apartment apartment) {
        if (StringUtils.isNotEmpty(apartment.getRoomNuber()) && !iApartmentService.checkRoomNuberUnique(apartment)) {
            return R.fail("新增宿舍'" + apartment.getDormitory() + "'失败，'" +apartment.getFloor() + "'楼层'" +apartment.getRoomNuber() +"'门牌号已存在");
        }
//        else if (!iApartmentService.checkFloorUnique(apartment))
//        {
//            return R.fail("新增宿舍'" + apartment.getDormitory() + "'失败，'" +apartment.getFloor() + "'楼层已存在");
//        }
        return toAjax(iApartmentService.updateByBo(apartment));
    }

    /**
     * 删除【公寓房间信息】
     *
     * @param ids 主键串
     */
    @SaCheckPermission("system:apartment:remove")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iApartmentService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
    /**
    * @Author cuihongmin
    * @Descripyion //宿舍楼字典查询
    * @return */
    @SaCheckLogin
    @GetMapping("/ssDictAll")

    public  R<List<ApartmentVo>> getSsDict(Apartment apartment) {

        return R.ok(iApartmentService.queryList1(apartment));
    }

    /**
     * @Author cuihongmin
     * @Descripyion //根据宿舍楼名称和楼层查询已存在宿舍楼层编号
     * @return */
    @SaCheckLogin
    @GetMapping("/sslFloor")

    public  R<List<Apartment>> getSslFloor(String dormitory,String floor) {

        return R.ok(iApartmentService.queryList2(dormitory,floor));
    }


    /**
     * 修改宿舍状态为维修中
     */
    @GetMapping("/updateStstus/{studentId}")
    public R<Void> edit1(@NotNull(message = "学号不能为空") @PathVariable Long studentId) {
        if (studentId != null) {
            return toAjax(iApartmentService.updateStatusById1(studentId));
        }
        return R.fail(501,"学号不能为空");

    }

    /**
     * 修改宿舍状态为已入住
     */
    @GetMapping("updateStatusById/{id}")
    public R<Void> edit2(@NotNull(message = "id不能为空") @PathVariable Integer id) {
        if (id != null) {
            return toAjax(iApartmentService.updateStatusById2(id));
        }
        return R.fail(501,"学生id不能为空");

    }




}
