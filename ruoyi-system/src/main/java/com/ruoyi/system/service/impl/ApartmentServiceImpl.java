package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.constant.ApartmentConstants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.ruoyi.system.domain.Student;
import com.ruoyi.system.mapper.StudentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.ApartmentBo;
import com.ruoyi.system.domain.vo.ApartmentVo;
import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.mapper.ApartmentMapper;
import com.ruoyi.system.service.IApartmentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-01
 */
@RequiredArgsConstructor
@Service
public class ApartmentServiceImpl implements IApartmentService {

    private final ApartmentMapper baseMapper;

    private final ApartmentMapper apartmentMapper;

    private final StudentMapper studentMapper;

    /**
     * 查询【请填写功能名称】
     */
    @Override
    public ApartmentVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询列表
     */
    @Override
    public TableDataInfo<ApartmentVo> queryPageList(ApartmentBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Apartment> lqw = buildQueryWrapper(bo);
        Page<ApartmentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询列表
     */
    @Override
    public List<ApartmentVo> queryList(ApartmentBo bo) {
        LambdaQueryWrapper<Apartment> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    /**
     * 查询字典列表
     */
    @Override
    public List<ApartmentVo> queryList1(Apartment apartment) {
        List<ApartmentVo> apartments = apartmentMapper.selectSslDictList();
        return apartments;
    }

    private LambdaQueryWrapper<Apartment> buildQueryWrapper(ApartmentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Apartment> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getDormitory()), Apartment::getDormitory, bo.getDormitory());
        lqw.eq(StringUtils.isNotBlank(bo.getRoomNuber()), Apartment::getRoomNuber, bo.getRoomNuber());
        lqw.eq(StringUtils.isNotBlank(bo.getFloor()), Apartment::getFloor, bo.getFloor());
        lqw.eq(StringUtils.isNotBlank(bo.getRoomType()), Apartment::getRoomType, bo.getRoomType());
        lqw.eq(bo.getBed() != null, Apartment::getBed, bo.getBed());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), Apartment::getStatus, bo.getStatus());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByBo(Apartment apartment) {
        Apartment add = BeanUtil.toBean(apartment, Apartment.class);
        validEntityBeforeSave(add);
        boolean flag = apartmentMapper.insertApartment(add);
        if (flag) {
            apartment.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(Apartment apartment) {
        Apartment update = BeanUtil.toBean(apartment, Apartment.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Apartment entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean checkFloorUnique(Apartment apartment) {
//        Long id = ObjectUtil.isNull(apartment.getId()) ? -1L : apartment.getId();
//        Apartment apartment1 = apartmentMapper.checkFloorUnique(apartment.getDormitory(), apartment.getFloor());
//        if (ObjectUtil.isNotNull(apartment1) && apartment1.getId().longValue() != id.longValue())
//        {
//            return ApartmentConstants.NOT_UNIQUE;
//        }
        return ApartmentConstants.UNIQUE;
    }

    @Override
    public boolean checkRoomNuberUnique(Apartment apartment) {
        Long id = ObjectUtil.isNull(apartment.getId()) ? -1L : apartment.getId();
        Apartment apartment1 = apartmentMapper.checkRoomNuberUnique(apartment.getDormitory(), apartment.getFloor(),apartment.getRoomNuber());
        if (ObjectUtil.isNotNull(apartment1) && apartment1.getId().longValue() != id.longValue())
        {
            return ApartmentConstants.NOT_UNIQUE;
        }
        return ApartmentConstants.UNIQUE;
    }

    @Override
    public List<Apartment> queryList2(String dormitory, String floor) {
        Map<String, Object> param = new HashMap<>();
        param.put("dormitory", dormitory);
        param.put("floor", floor);
        if (floor == null) {
            List<Apartment> apartments = apartmentMapper.selectApartmentList(param);
            return apartments;
        }else {
            List<Apartment> apartments = apartmentMapper.selectApartmentList1(param);
            return apartments;
        }

//        List<Apartment> apartments = apartmentMapper.selectApartmentList(param);

    }

    @Override
    public Boolean updateStatusById1(Long studentId) {
        // 根据学号查询该生的宿舍信息
        Student student = studentMapper.selectStudentList(studentId);
        String dormitory = student.getDormitory();
        String floor = student.getFloor();
        Long roomNuber = student.getDormitoryNumber();
        // 根据宿舍信息修改宿舍状态
        Boolean flage = apartmentMapper.updateStatusByFloor(dormitory, floor, roomNuber.toString())>0;
        return flage;
    }

    @Override
    public Boolean updateStatusById2(Integer id) {
        boolean flage = apartmentMapper.updateStatusById(id) > 0;
        return flage;
    }
}
