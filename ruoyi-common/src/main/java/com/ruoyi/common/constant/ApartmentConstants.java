package com.ruoyi.common.constant;

/**
 * @Descripttion 宿舍常量信息
 * @Author cuihongmin
 * @Date 2023/12/2 15:34
 */
public class ApartmentConstants {

    /** 校验是否唯一的返回标识 */
    public final static boolean UNIQUE = true;
    public final static boolean NOT_UNIQUE = false;
}
