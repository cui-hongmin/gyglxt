package com.ruoyi.system.domain.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】业务对象 student
 *
 * @author ruoyi
 * @date 2023-11-28
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class StudentBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Integer id;

    /**
     * 学号
     */
    @NotNull(message = "学号不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long studentId;

    /**
     * 姓名
     */
    @NotBlank(message = "姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 学生状态
     */
    @NotBlank(message = "学生状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 年龄
     */
    @NotNull(message = "年龄不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long age;

    /**
     * 性别
     */
//    @NotBlank(message = "性别不能为空", groups = { AddGroup.class, EditGroup.class })
    private String sex;

    /**
     * 宿舍楼
     */
    @NotBlank(message = "宿舍楼不能为空", groups = { AddGroup.class, EditGroup.class })
    private String dormitory;

    /**
     * 楼层
     */
    @NotBlank(message = "楼层不能为空", groups = { AddGroup.class, EditGroup.class })
    private String floor;

    /**
     * 寝室编号
     */
    @NotNull(message = "寝室编号不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long dormitoryNumber;

    /**
     * 电话
     */
    @NotBlank(message = "电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String phone;

    /**
     * 邮箱
     */
    @NotBlank(message = "邮箱不能为空", groups = { AddGroup.class, EditGroup.class })
    private String email;

    /**
     * 院系
     */
    @NotBlank(message = "院系不能为空", groups = { AddGroup.class, EditGroup.class })
    private String department;

    /**
     * 专业
     */
    @NotBlank(message = "专业不能为空", groups = { AddGroup.class, EditGroup.class })
    private String speciality;

    /**
     * 班级
     */
    @NotBlank(message = "班级不能为空", groups = { AddGroup.class, EditGroup.class })
    private String classStudent;


}
