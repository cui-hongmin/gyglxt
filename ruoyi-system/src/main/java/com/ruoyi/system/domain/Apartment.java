package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 apartment
 *
 * @author ruoyi
 * @date 2023-12-01
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("apartment")
public class Apartment extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
//    @TableId(value = "id")
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 门牌号(房间编号)
     */
    private String roomNuber;

    /**
     * 宿舍楼名称
     */
    private String dormitory;
    /**
     * 楼层
     */
    private String floor;
    /**
     * 房间类型(四人间、八人间)
     */
    private String roomType;
    /**
     * 床位数
     */
    private Long bed;
    /**
     * 剩余床位
     */
    private Integer sycw;
    /**
     * 状态(0：空闲、1：已入住、2：维修中）
     */
    private String status;


    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;

}
