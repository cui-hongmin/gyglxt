package com.ruoyi.system.service;

import com.ruoyi.system.domain.Visitor;
import com.ruoyi.system.domain.vo.VisitorVo;
import com.ruoyi.system.domain.bo.VisitorBo;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author ruoyi
 * @date 2023-12-05
 */
public interface IVisitorService {

    /**
     * 查询【请填写功能名称】
     */
    VisitorVo queryById(Long id);

    /**
     * 查询【请填写功能名称】列表
     */
    TableDataInfo<Visitor> queryPageList(VisitorBo bo, PageQuery pageQuery);

    /**
     * 查询【请填写功能名称】列表
     */
    List<VisitorVo> queryList(VisitorBo bo);

    /**
     * 新增【请填写功能名称】
     */
    Boolean insertByVisitor(Visitor visitor);

    /**
     * 修改【请填写功能名称】
     */
    Boolean updateByBo(VisitorBo bo);

    /**
     * 校验并批量删除【请填写功能名称】信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
