package com.ruoyi.system.domain.bo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】业务对象 apartment
 *
 * @author ruoyi
 * @date 2023-12-01
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ApartmentBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Integer id;

    /**
     * 宿舍楼名称
     */
    @NotNull(message = "宿舍楼名称不能为空", groups = { EditGroup.class })
    private String dormitory;

    /**
     * 门牌号(房间编号)
     */
    @NotBlank(message = "门牌号(房间编号)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String roomNuber;

    /**
     * 楼层
     */
    @NotBlank(message = "楼层不能为空", groups = { AddGroup.class, EditGroup.class })
    private String floor;

    /**
     * 房间类型(四人间、八人间)
     */
    @NotBlank(message = "房间类型(四人间、八人间)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String roomType;

    /**
     * 床位数
     */
    @NotNull(message = "床位数不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long bed;

    /**
     * 剩余床位
     */
    @NotBlank(message = "剩余床位", groups = { AddGroup.class, EditGroup.class })
    private Integer sycw;

    /**
     * 状态(0：空闲、1：已入住、2：维修中）
     */
    @NotBlank(message = "状态(0：空闲、1：已入住、2：维修中）不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;




}
