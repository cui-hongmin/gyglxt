package com.ruoyi.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.bo.VisitorBo;
import com.ruoyi.system.domain.vo.VisitorVo;
import com.ruoyi.system.domain.Visitor;
import com.ruoyi.system.mapper.VisitorMapper;
import com.ruoyi.system.service.IVisitorService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author ruoyi
 * @date 2023-12-05
 */
@RequiredArgsConstructor
@Service
public class VisitorServiceImpl implements IVisitorService {

    private final VisitorMapper baseMapper;

    private final VisitorMapper visitorMapper;


    /**
     * 查询【请填写功能名称】
     */
    @Override
    public VisitorVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public TableDataInfo<Visitor> queryPageList(VisitorBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Visitor> lqw = buildQueryWrapper(bo);
        Page<Visitor> result = baseMapper.selectVoPage(pageQuery.build(), lqw,Visitor.class);
        return TableDataInfo.build(result);
    }

    /**
     * 查询【请填写功能名称】列表
     */
    @Override
    public List<VisitorVo> queryList(VisitorBo bo) {
        LambdaQueryWrapper<Visitor> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Visitor> buildQueryWrapper(VisitorBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Visitor> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getFkName()), Visitor::getFkName, bo.getFkName());
        lqw.eq(StringUtils.isNotBlank(bo.getFkPhone()), Visitor::getFkPhone, bo.getFkPhone());
        lqw.like(StringUtils.isNotBlank(bo.getXfName()), Visitor::getXfName, bo.getXfName());
        return lqw;
    }

    /**
     * 新增【请填写功能名称】
     */
    @Override
    public Boolean insertByVisitor(Visitor visitor) {
        Visitor add = BeanUtil.toBean(visitor, Visitor.class);
        validEntityBeforeSave(add);
        boolean flag = visitorMapper.insertVisitor(add);
        if (flag) {
            visitor.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改【请填写功能名称】
     */
    @Override
    public Boolean updateByBo(VisitorBo bo) {
        Visitor update = BeanUtil.toBean(bo, Visitor.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Visitor entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除【请填写功能名称】
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
