package com.ruoyi.system.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.ruoyi.common.annotation.ExcelDictFormat;
import com.ruoyi.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 【请填写功能名称】视图对象 apartment
 *
 * @author ruoyi
 * @date 2023-12-01
 */
@Data
@ExcelIgnoreUnannotated
public class ApartmentVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Integer id;

    /**
     * 宿舍楼名称
     */
    @ExcelProperty(value = "宿舍楼名称")
    private String dormitory;

    /**
     * 门牌号(房间编号)
     */
    @ExcelProperty(value = "门牌号(房间编号)")
    private String roomNuber;

    /**
     * 楼层
     */
    @ExcelProperty(value = "楼层")
    private String floor;

    /**
     * 房间类型(四人间、八人间)
     */
    @ExcelProperty(value = "房间类型(四人间、八人间)")
    private String roomType;

    /**
     * 床位数
     */
    @ExcelProperty(value = "床位数")
    private Long bed;

    /**
     * 剩余床位
     */
    @ExcelProperty(value = "剩余床位")
    private Integer sycw;

    /**
     * 状态(0：空闲、1：已入住、2：维修中）
     */
    @ExcelProperty(value = "状态(0：空闲、1：已入住、2：维修中）")
    private String status;




}
