package com.ruoyi.web.controller.system;

/**
 * @Descripttion 文件上传
 * @Author cuihongmin
 * @Date 2023/11/29 15:41
 */

import cn.hutool.core.util.ObjectUtil;
import com.ruoyi.common.core.domain.R;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 文件的上传和下载
 */
@RestController
@RequestMapping("/common")
public class SysFileController {
    private static final String UPLOAD_PATH = "D:/UploadImage/";
    private static final String DOWNLOAD_PATH = "D:/DowImage/";

    @PostMapping("/upload")
//    @ResponseBody
    public R<Map<String, String>> upload(@RequestParam("file") MultipartFile file) {
        //不可以上传空文件
        if (ObjectUtil.isNull(file)) {
            return R.fail("上传文件不能为空");
        }
//        if(file.isEmpty()) {
//            return "文件为空，请选择文件后再上传";
//        }

        String filename = file.getOriginalFilename();
        String suffix = filename.substring(filename.lastIndexOf("."));
        filename = UPLOAD_PATH + UUID.randomUUID() + suffix;
        File dest = new File(filename);

        //若文件已存在则不执行保存操作
        try {
            if(!dest.exists()) {
                file.transferTo(dest);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("上传失败");
        }
        Map<String, String> map = new HashMap<>(2);
        map.put("url", "");
//        map.put("fileName", oss.getOriginalName());
        map.put("ossId", filename);

        return R.ok(map);
    }


    @GetMapping("/download")
    @ResponseBody
    public String download(HttpServletResponse response, String ossId) {
        System.out.println("kkk");
        System.out.println(ossId);

        System.out.println("lll");

        File file = new File(ossId);
        if (!file.exists()) {
            return "所访问的资源不存在";
        }

        try {
            FileInputStream fis = new FileInputStream(file);
            // 设置相关格式
            response.setContentType("application/force-download");
            // 设置下载后的文件名以及header
            response.addHeader("Content-disposition", "attachment;fileName=" + file.getName());
            OutputStream os = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len = 0;
            while((len = fis.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
            return "下载失败";
        }

        return "开始下载";
    }



}
