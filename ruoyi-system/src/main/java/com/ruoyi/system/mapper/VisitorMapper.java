package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.Visitor;
import com.ruoyi.system.domain.vo.VisitorVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-05
 */
public interface VisitorMapper extends BaseMapperPlus<VisitorMapper, Visitor, VisitorVo> {

    /**
     * 新增访客信息
     *
     * @param visitor 访客信息
     * @return 结果
     */
    public boolean insertVisitor(Visitor visitor);
}
