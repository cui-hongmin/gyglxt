package com.ruoyi.system.domain.bo;

import com.ruoyi.common.core.validate.AddGroup;
import com.ruoyi.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】业务对象 visitor
 *
 * @author ruoyi
 * @date 2023-12-05
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class VisitorBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Integer id;

    /**
     * 访客姓名
     */
    @NotBlank(message = "访客姓名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String fkName;

    /**
     * 访客联系电话
     */
    @NotBlank(message = "访客联系电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String fkPhone;

    /**
     * 寻访人
     */
    @NotBlank(message = "寻访人不能为空", groups = { AddGroup.class, EditGroup.class })
    private String xfName;

//
//    /**
//     * 创建时间
//     */
//    @NotBlank(message = "创建时间不能为空", groups = { AddGroup.class, EditGroup.class })
//    private String createTime;



}
