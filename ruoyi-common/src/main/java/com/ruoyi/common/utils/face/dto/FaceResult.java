package com.ruoyi.common.utils.face.dto;



import com.alibaba.fastjson2.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
/**
 * @Descripttion
 * @Author cuihongmin
 * @Date 2023/12/7 10:27
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FaceResult implements Serializable{
    private String logId;

    private String errorMsg;

    private int cached;

    private int errorCode;

    private long timestamp;

    private JSONObject data;

    public boolean isSuccess() {
        return 0 == this.errorCode ? true : false;

    }
}
