package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.Apartment;
import com.ruoyi.system.domain.Student;
import com.ruoyi.system.domain.vo.ApartmentVo;
import com.ruoyi.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author ruoyi
 * @date 2023-12-01
 */
public interface ApartmentMapper extends BaseMapperPlus<ApartmentMapper, Apartment, ApartmentVo> {
    /**
     * 新增宿舍信息
     *
     * @param apartment 宿舍信息
     * @return 结果
     */
    public boolean insertApartment(Apartment apartment);


    /**
     * 查询字典列表
     *
     * @param
     * @return 结果
     */
    public List<ApartmentVo> selectSslDictList();

    /**
     * 根据宿舍楼名称和楼层、门牌号去查询宿舍信息
     *
     * @param
     * @return 结果
     */
    public ApartmentVo selectApartment(@Param("dormitory")String dormitory,@Param("floor")String floor );

    /**
     * 校验宿舍下宿舍楼层是否唯一
     *
     * @param param 参数
     * @return 结果
     */
    public Apartment selectApartByFloor(Map<String,Object> param );



    /**
     * 校验宿舍下宿舍楼层下的门牌号是否唯一
     *
     * @param dormitory 宿舍楼名称  floor 楼层 roomNuber 门牌号
     * @return 结果
     */
    public Apartment checkRoomNuberUnique(@Param("dormitory")String dormitory,@Param("floor")String floor,@Param("roomNuber")String roomNuber );


    public List<Apartment> selectApartmentList(Map<String,Object> param);

    public List<Apartment> selectApartmentList1(Map<String,Object> param);

    // 根据id去修改剩余床位
    public int updateSycwById(@Param("id")Integer id,@Param("sycw")Integer sycw);

    // 根据宿舍楼、楼层、门牌号修改宿舍状态为---维修中
    public int updateStatusByFloor(@Param("dormitory")String dormitory,@Param("floor")String floor,@Param("roomNuber")String roomNuber);

    public int updateStatusById(@Param("id")Integer id);
}
